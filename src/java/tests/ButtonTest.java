package tests;

import lib.CoreTestCase;
import lib.ui.MainScreenPageObject;
import lib.ui.NavigationUI;
import lib.ui.SettingsPageObject;
import lib.ui.factories.MainScreenPageObjectFactory;
import lib.ui.factories.NavigationUIFactory;
import lib.ui.factories.SettingsPageObjectFactory;
import org.junit.Test;

public class ButtonTest extends CoreTestCase {
    NavigationUI navigationUI;
    MainScreenPageObject mainScreenPageObject;
    SettingsPageObject settingsPageObject;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        navigationUI = NavigationUIFactory.get(driver);
        mainScreenPageObject = MainScreenPageObjectFactory.get(driver);
        settingsPageObject = SettingsPageObjectFactory.get(driver);
    }

    @Test
    public void testSimpleTest(){
        navigationUI.clickOnSkipBtn();
        mainScreenPageObject.waitForAboutBtn();
        mainScreenPageObject.clickOnAboutBtn();
        mainScreenPageObject.waitForAboutInfoBtn();
        this.clickToSystemBack();
        navigationUI.waitForMainScreenOpens(); 
    }
}
