package suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tests.ButtonTest;
import tests.SecondClass;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        ButtonTest.class,
        SecondClass.class
})

public class TestSuite {}