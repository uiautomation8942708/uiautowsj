package lib;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.URL;

public class Platform {
    private static final String
            APPIUM_URL = "http://127.0.0.1:4723/wd/hub",
            PLATFORM_ANDROID = "android",
            ANDROID_PLATFORM_V = "10",
            PLATFORM_IOS = "ios",
            APP_PACKAGE = "wsj.reader_sp",
            APP_ACTIVITY = "wsj.ui.IssueActivity",
            APP_LOCATION = "/Users/bogoutdinovruslan/IdeaProjects/bankProject/UIAutomationProject/apk/the-wall-street-journal-5-17-0-1.apk";
    private static Platform instance;

    private Platform() {
    }

    public static Platform getInstance() {
        if (instance == null) {
            instance = new Platform();
        }
        return instance;
    }

    public AppiumDriver getDriver() throws Exception {
        URL URL = new URL(APPIUM_URL);
        if (this.isAndroid()) {
            return new AndroidDriver(URL, this.getAndroidDesireCapabilities());
        } else if (this.isIOS()) {
            return new IOSDriver(URL, this.getIOSDesiredCapabilities());
        } else {
            throw new Exception("Cannot detect the type of driver. Platform value: " + this.getPlatformVar());
        }
    }

    public boolean isAndroid() {
        return isPlatform(PLATFORM_ANDROID);
    }

    public boolean isIOS() {
        return isPlatform(PLATFORM_IOS);
    }

    private DesiredCapabilities getAndroidDesireCapabilities() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("deviceName", "AndroidDevice");
        capabilities.setCapability("platformVersion", ANDROID_PLATFORM_V);
        capabilities.setCapability("automationName", "Appium");
        capabilities.setCapability("appPackage", APP_PACKAGE);
        capabilities.setCapability("appActivity", APP_ACTIVITY);
        capabilities.setCapability("app", APP_LOCATION);
        capabilities.setCapability("appium:ignoreHiddenApiPolicyError", true);
        capabilities.setCapability("autoGrantPermissions", true);

        if (this.isAndroid()) {
            capabilities.setCapability("noReset", "false");
        } else {
            capabilities.setCapability("noReset", "false");
        }
        return capabilities;
    }

    private DesiredCapabilities getIOSDesiredCapabilities() {
        // Here should be declared iOS capabilities
        DesiredCapabilities capabilities = new DesiredCapabilities();
        return capabilities;
    }

    private boolean isPlatform(String myPlatform) {
        String platform = this.getPlatformVar();
        return myPlatform.equals(platform);
    }

    private String getPlatformVar() {
        return System.getenv("PLATFORM");
    }
}
