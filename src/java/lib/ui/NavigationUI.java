package lib.ui;

import io.appium.java_client.AppiumDriver;

public class NavigationUI extends MainPageObject {
    public static String
            MAIN_LOGO,
            SKIP_BTN;
    public NavigationUI(AppiumDriver driver) {
        super(driver);
    }

    public void waitForMainScreenOpens(){
        this.waitForElementPresent(
                MAIN_LOGO,
                "The main screen hasn't been opened",
                10
        );
    }

    public void clickOnSkipBtn(){
        this.waitForElementAndClick(
                SKIP_BTN,
                "Cannot find and click on 'Skip' button",
                5
        );
    }
}
