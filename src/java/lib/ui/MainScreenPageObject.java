package lib.ui;

import io.appium.java_client.AppiumDriver;

public class MainScreenPageObject extends MainPageObject {
    public static String
            SETTINGS_BTN,
            ABOUT_BTN,
            ABOUT_INF_BTN;
    public MainScreenPageObject(AppiumDriver driver) {
        super(driver);
    }

    public void waitForAboutBtn(){
        this.waitForElementPresent(
                ABOUT_BTN,
                "Cannot find and click on 'About' button",
                10
        );
    }

    public void waitForAboutInfoBtn(){
        this.waitForElementPresent(
                ABOUT_INF_BTN,
                "Cannot find and click on 'About info' button",
                15
        );
    }

    public void clickOnAboutBtn(){
        this.waitForElementAndClick(
                ABOUT_BTN,
                "Cannot click on 'About' button",
                10
        );
    }
}
