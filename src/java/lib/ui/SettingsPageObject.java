package lib.ui;

import io.appium.java_client.AppiumDriver;

public class SettingsPageObject extends MainPageObject{
    public static String
            SETTINGS_CONTAINER;
    public SettingsPageObject(AppiumDriver driver) {
        super(driver);
    }

    public void waitForSettingContainer(){
        this.waitForElementPresent(
                SETTINGS_CONTAINER,
                "Cannot find settings container",
                10
        );
    }
}
