package lib.ui.ios;

import io.appium.java_client.AppiumDriver;
import lib.ui.NavigationUI;

public class iOSNavigationUI extends NavigationUI {
    static {
        // Here should be declared iOS locators
    }

    public iOSNavigationUI(AppiumDriver driver) {
        super(driver);
    }
}
