package lib.ui.android;

import io.appium.java_client.AppiumDriver;
import lib.ui.NavigationUI;

public class AndroidNavigationUI extends NavigationUI {

    static {
        MAIN_LOGO = "id:wsj.reader_sp:id/logo";
        SKIP_BTN = "xpath://*[@text='Skip']";
    }

    public AndroidNavigationUI(AppiumDriver driver) {
        super(driver);
    }
}
