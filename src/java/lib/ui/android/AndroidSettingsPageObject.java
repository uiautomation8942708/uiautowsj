package lib.ui.android;

import io.appium.java_client.AppiumDriver;
import lib.ui.SettingsPageObject;

public class AndroidSettingsPageObject extends SettingsPageObject {
    static {
        SETTINGS_CONTAINER = "id:bbc.mobile.news.ww:id/settings_container";
    }
    public AndroidSettingsPageObject(AppiumDriver driver) {
        super(driver);
    }
}
