package lib.ui.android;

import io.appium.java_client.AppiumDriver;
import lib.ui.MainScreenPageObject;
import lib.ui.NavigationUI;

public class AndroidMainScreenPageObject extends MainScreenPageObject {
    static {
        SETTINGS_BTN = "id:bbc.mobile.news.ww:id/discovery_header_ic_left";
        ABOUT_BTN = "id:wsj.reader_sp:id/nav_drawer_about_row";
        ABOUT_INF_BTN = "id:wsj.reader_sp:id/about_app_info";
    }

    public AndroidMainScreenPageObject(AppiumDriver driver) {
        super(driver);
    }
}
