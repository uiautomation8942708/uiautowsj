package lib.ui.factories;

import io.appium.java_client.AppiumDriver;
import lib.Platform;
import lib.ui.android.AndroidMainScreenPageObject;

public class MainScreenPageObjectFactory {
    public static AndroidMainScreenPageObject get(AppiumDriver driver) {
        if (Platform.getInstance().isAndroid()) {
            return new AndroidMainScreenPageObject(driver);
        } else {
            //return new iOSNavigationUI(driver);
        }
        return null;
    }
}
